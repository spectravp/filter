<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/16/14
 * Time: 2:59 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;


/**
 * Class BooleanToInteger
 * @package Filter
 */
class BooleanToInteger extends AbstractFilter {

    /**
     * @var int
     */
    protected $trueValue = 1;
    /**
     * @var int
     */
    protected $falseValue = 0;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $data
     * @return int
     */
    public function filter($data)
    {
        if( is_int($data) )
        {
            return $data;
        }

        if( $data == true || $data == 'true' )
        {
            return $this->getTrueValue();
        }

        return $this->getFalseValue();
    }

    /**
     * @return int
     */
    public function getFalseValue()
    {
        return $this->falseValue;
    }

    /**
     * @param int $falseValue
     */
    public function setFalseValue($falseValue)
    {
        $this->falseValue = $falseValue;
    }

    /**
     * @return int
     */
    public function getTrueValue()
    {
        return $this->trueValue;
    }

    /**
     * @param int $trueValue
     */
    public function setTrueValue($trueValue)
    {
        $this->trueValue = $trueValue;
    }
} 