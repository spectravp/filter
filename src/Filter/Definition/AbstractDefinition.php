<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/1/14
 * Time: 7:29 PM
 */

namespace Filter\Definition;

use Zend\Filter\FilterInterface;

abstract class AbstractDefinition implements DefinitionInterface, FilterInterface {

    /**
     * Filters the data to the string
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        foreach($this->getArray() as $key => $string)
        {
            if( $key == $value )
            {
                return $string;
            }
        }
    }
} 