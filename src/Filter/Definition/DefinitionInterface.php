<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/1/14
 * Time: 7:29 PM
 */
namespace Filter\Definition;

interface DefinitionInterface {

    /**
     * Gets the definitions as an array
     * @return array
     */
    public function getArray();
}