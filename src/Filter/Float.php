<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/30/13
 * Time: 4:03 PM
 */
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Float
 * @package Filter
 */
class Float extends ToFloat {


    /**
     * @param $value
     * @return float
     */
    public function filter($value)
    {
        trigger_error('Float is deprecated. See ToFloat', E_USER_DEPRECATED);
        return parent::filter($value);
    }
}