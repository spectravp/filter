<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 2:50 PM
 * Project: filter
 */
namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Json\Decoder;

/**
 * Class JsonDecode
 * @package Filter
 */
class JsonDecode extends AbstractFilter {

    /**
     * @param $data
     * @return mixed
     */
    public function filter($data)
    {
        if( $data == null || $data == '' )
        {
            return array();
        }

        if( ! is_string($data) )
        {
            // data is not a string, it must be an object or array already!
            return $data;
        }

        return Decoder::decode($data);
    }
} 