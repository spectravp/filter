<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda 
 * Date: 12/3/14 9:49 AM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */

namespace Filter;
use Zend\Filter\AbstractFilter;


/**
 * Class States
 * @package Filter
 */
class States extends AbstractFilter {

    /**
     * States
     * @var array
     */
    protected $states = array(
                        'AL'=>'Alabama',
                        'AK'=>'Alaska',
                        'AZ'=>'Arizona',
                        'AR'=>'Arkansas',
                        'CA'=>'California',
                        'CO'=>'Colorado',
                        'CT'=>'Connecticut',
                        'DE'=>'Delaware',
                        'DC'=>'District of Columbia',
                        'FL'=>'Florida',
                        'GA'=>'Georgia',
                        'HI'=>'Hawaii',
                        'ID'=>'Idaho',
                        'IL'=>'Illinois',
                        'IN'=>'Indiana',
                        'IA'=>'Iowa',
                        'KS'=>'Kansas',
                        'KY'=>'Kentucky',
                        'LA'=>'Louisiana',
                        'ME'=>'Maine',
                        'MD'=>'Maryland',
                        'MA'=>'Massachusetts',
                        'MI'=>'Michigan',
                        'MN'=>'Minnesota',
                        'MS'=>'Mississippi',
                        'MO'=>'Missouri',
                        'MT'=>'Montana',
                        'NE'=>'Nebraska',
                        'NV'=>'Nevada',
                        'NH'=>'New Hampshire',
                        'NJ'=>'New Jersey',
                        'NM'=>'New Mexico',
                        'NY'=>'New York',
                        'NC'=>'North Carolina',
                        'ND'=>'North Dakota',
                        'OH'=>'Ohio',
                        'OK'=>'Oklahoma',
                        'OR'=>'Oregon',
                        'PA'=>'Pennsylvania',
                        'RI'=>'Rhode Island',
                        'SC'=>'South Carolina',
                        'SD'=>'South Dakota',
                        'TN'=>'Tennessee',
                        'TX'=>'Texas',
                        'UT'=>'Utah',
                        'VT'=>'Vermont',
                        'VA'=>'Virginia',
                        'WA'=>'Washington',
                        'WV'=>'West Virginia',
                        'WI'=>'Wisconsin',
                        'WY'=>'Wyoming');

    /**
     * @var string
     */
    protected $keyToMatch = 'abbreviation';

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function filter($value)
    {
        if( $this->keyToMatch == 'abbreviation' )
        {
            if(array_key_exists($value, $this->states))
            {
                return $this->states[$value];
            }
        }
        elseif( $this->keyToMatch == 'name' )
        {
            if( in_array($value, $this->states) )
            {
                return $this->states[array_search($value, $this->states)];
            }
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getKeyToMatch()
    {
        return $this->keyToMatch;
    }

    /**
     * @param string $keyToMatch
     */
    public function setKeyToMatch($keyToMatch)
    {
        $this->keyToMatch = $keyToMatch;
    }

    /**
     * @return array
     */
    public function getForSelect()
    {
        $array = array();

        foreach($this->states as $key => $value)
        {
            $array[] = array('value'=>$key, 'label'=>$value);
        }

        return $array;
    }
} 