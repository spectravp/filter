<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda
 * Date: 7/16/18 2:59 PM
 *
 * (c) Copyright 2018 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;

/**
 * Class DefaultValue
 * @package Filter
 */
class DefaultValue extends AbstractFilter
{
    /**
     * @var mixed
     */
    protected $defaultValue;

    /**
     * DefaultValue constructor.
     * @param null $options
     */
    public function __construct($options = null)
    {
        if( $options !== null )
        {
            $this->setOptions($options);
        }
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if( $value === null )
        {
            return $this->getDefaultValue();
        }

        return $value;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @param mixed $value
     */
    public function setDefaultValue($value)
    {
        $this->defaultValue = $value;
    }
}
