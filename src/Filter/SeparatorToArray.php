<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/31/14
 * Time: 11:20 AM
 * Project: social-chinese-web
 */
namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class QueryParamToArray
 * @package Filter
 */
class SeparatorToArray extends AbstractFilter {

    /**
     * @var string
     */
    protected $separator = ',';

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @param $param
     * @return array
     */
    public function filter($param)
    {
        if ($param == '' || $param == null)
        {
            return array();
        }

        if( ! stristr($param, $this->getSeparator()))
        {
            return array($param);
        }

        $parts =  explode($this->getSeparator(), $param);

        foreach($parts as $key => $part)
        {
            $parts[$key] = trim($part);
        }

        return $parts;
    }
}