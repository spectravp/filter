<?php
namespace Filter;

use PHPThumb\GD;
use Zend\Filter\AbstractFilter;

/**
 * Class Image
 * @package Filter
 */
class Image extends AbstractFilter {

    /**
     * Thumbnail Constant
     */
    const THUMBNAIL = 'thumb';

    /**
     * Crop Constant
     */
    const CROP = 'crop';

    /**
     * Crop Constant for x-y-w-h cropping
     */
    const CROP_SEGMENT = 'crop-segment';

    /**
     * Mode to use when adjusting the image
     * @var string
     */
    protected $mode = 'thumb';

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * The source image directory
     * Not always needed as the full path may be passed into the filter() function
     * @var string
     */
    protected $sourceDirectory;

    /**
     * The destination directory to store the image in
     * @var string
     */
    protected $destinationDirectory;

    /**
     * The destination file name
     * @var string
     */
    protected $destinationFileName;

    /**
     * @var array
     */
    protected $adapterOptions = array();

    /**
     * Flag for whether this should append the resize mode and size to the filename
     * @var bool
     */
    protected $appendModeAndSize = false;

    /**
     * @var int
     */
    protected $startX = 0;

    /**
     * @var int
     */
    protected $startY = 0;

    /**
     * Flag for throwing exceptions
     * @var bool
     */
    protected $throwExceptions = true;

    /**
     * Default image path if not found.
     * @var string
     */
    protected $defaultImage = null;

    /**
     * @param $fileName
     * @param array $options
     * @param array $plugins
     * @return GD
     */
    public function create($fileName, $options = array(), array $plugins = array())
    {
        return new GD($fileName, $options, $plugins);
    }

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param mixed $filePath
     * @return string
     * @throws \Exception
     */
    public function filter($filePath)
    {
        try {
            if( is_array($filePath) && isset($filePath['tmp_name']))
            {
                $filePath = array_pop(explode('/', $filePath['tmp_name']));
            }

            if( $this->getDestinationFileName() )
            {
                $filePath = $this->getDestinationFileName();
            }

            $filePath = $this->processFileName($filePath);

            if (file_exists($this->getDestinationDirectory() . $filePath)) {
                return  $this->getDestinationDirectory() . $filePath;
            }

            $resource = $this->create($this->getSourceDirectory() . $filePath, $this->getAdapterOptions());

            switch( $this->getMode() )
            {
                case 'crop':
                {
                    $resource->adaptiveResize($this->getWidth(), $this->getHeight());
                    break;
                }

                case 'crop-segment':
                {
                    $resource->crop($this->getStartX(), $this->getStartY(), $this->getWidth(), $this->getHeight());
                    break;
                }

                case 'thumb':
                default:
                {
                    $resource->resize($this->getWidth(), $this->getHeight());
                }
            }

            $resource->save($this->getDestinationDirectory() . $filePath);

            return  $this->getDestinationDirectory() . $filePath;
        }
        catch(\Exception $e)
        {
            if( $this->throwExceptions === false || $this->defaultImage != null )
            {
                return $this->getDefaultImage();
            }

            throw $e;
        }
    }


    /**
     * Gets the directory of the source image
     * @return string
     */
    public function getSourceDirectory()
    {
        return $this->sourceDirectory;
    }

    /**
     * Sets the directory of the source image
     * @param string $target
     */
    public function setSourceDirectory($target)
    {
        $target = str_ireplace(array('/', '\\'), DIRECTORY_SEPARATOR, $target);

        if( substr($target, -1) != DIRECTORY_SEPARATOR )
        {
            $target .= DIRECTORY_SEPARATOR;
        }

        $this->sourceDirectory = $target;
    }

    /**
     * Gets the destination directory
     * @return string
     */
    public function getDestinationDirectory()
    {
        return $this->destinationDirectory;
    }

    /**
     * Sets the destination directory
     * @param string $destinationDirectory
     */
    public function setDestinationDirectory($destinationDirectory)
    {
        if( substr($destinationDirectory, -1) != DIRECTORY_SEPARATOR )
        {
            $destinationDirectory .= DIRECTORY_SEPARATOR;
        }

        $this->destinationDirectory = $destinationDirectory;
    }

    /**
     * Gets the destination file name
     * @return string
     */
    public function getDestinationFileName()
    {
        return $this->destinationFileName;
    }

    /**
     * @param string $destinationFileName
     */
    public function setDestinationFileName($destinationFileName)
    {
        $this->destinationFileName = $destinationFileName;
    }

    /**
     * Will process the file name and append mode and size if necessary
     * @param $filePath
     * @return string
     */
    public function processFileName($filePath)
    {
        if( $this->getAppendModeAndSize() )
        {
            $fileParts = explode('.', $filePath);

            if(stristr($fileParts[0], DIRECTORY_SEPARATOR) )
            {
                $fileParts[0] = array_pop(explode(DIRECTORY_SEPARATOR, $fileParts[0]));
            }

            return $fileParts[0] . '-' . $this->getMode() . '-' . $this->getWidth() . 'x' . $this->getHeight() . '.' . $fileParts[1];
        }

        return $filePath;
    }

    /**
     * @return array
     */
    public function getAdapterOptions()
    {
        return $this->adapterOptions;
    }

    /**
     * @param array $adapterOptions
     */
    public function setAdapterOptions($adapterOptions)
    {
        $this->adapterOptions = $adapterOptions;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * Accepts a file size as a string
     * @param string $size
     * @throws \Exception
     */
    public function setSize($size)
    {
        $parts = explode('x', strtolower($size));

        if( ! is_array($parts))
        {
            throw new \Exception('Could not process image size');
        }

        $this->setWidth($parts[0]);
        $this->setHeight($parts[1]);
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param mixed $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return boolean
     */
    public function getAppendModeAndSize()
    {
        return $this->appendModeAndSize;
    }

    /**
     * @param boolean $appendModeAndSize
     */
    public function setAppendModeAndSize($appendModeAndSize)
    {
        $this->appendModeAndSize = $appendModeAndSize;
    }

    /**
     * @return int
     */
    public function getStartX()
    {
        return $this->startX;
    }

    /**
     * @param int $startX
     */
    public function setStartX($startX)
    {
        $this->startX = $startX;
    }

    /**
     * @return int
     */
    public function getStartY()
    {
        return $this->startY;
    }

    /**
     * @param int $startY
     */
    public function setStartY($startY)
    {
        $this->startY = $startY;
    }

    /**
     * @return boolean
     */
    public function getThrowExceptions()
    {
        return $this->throwExceptions;
    }

    /**
     * @param boolean $throwExceptions
     */
    public function setThrowExceptions($throwExceptions)
    {
        $this->throwExceptions = $throwExceptions;
    }

    /**
     * @return string
     */
    public function getDefaultImage()
    {
        return $this->defaultImage;
    }

    /**
     * @param string $defaultImage
     */
    public function setDefaultImage($defaultImage)
    {
        $this->defaultImage = $defaultImage;
    }
}