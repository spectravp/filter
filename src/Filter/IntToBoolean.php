<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/30/15
 * Time: 9:49 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

/**
 * Class IntToBoolean
 * @package Filter
 */
class IntToBoolean extends AbstractFilter {

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return boolean
     */
    public function filter($value)
    {
        if( is_bool($value) )
        {
            return $value;
        }

        if( is_int($value) && $value > 0 )
        {
            return true;
        }
        elseif( is_string($value) )
        {
            if( $value === '0' )
            {
                return false;
            }

            return true;
        }

        return false;
    }
} 