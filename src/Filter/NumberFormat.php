<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 3/11/14
 * Time: 8:17 AM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;


/**
 * Class NumberFormat
 * @package Filter
 */
class NumberFormat extends AbstractFilter {

    /**
     * @var int
     */
    private $decimals = 2;
    /**
     * @var string
     */
    private $decimalPoint = '.';
    /**
     * @var string
     */
    private $thousandsSeparator = ',';

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        foreach($options as $key => $value)
        {
            switch($key)
            {
                case 'decimals':
                {
                    $this->decimals = $value;
                    break;
                }
                case 'decimal_point':
                {
                    $this->decimalPoint = $value;
                    break;
                }
                case 'thousands_separator':
                {
                    $this->thousandsSeparator = $value;
                    break;
                }
            }
        }
    }

    /**
     * Filters data into a number formatted string
     * @param mixed $value
     * @return mixed|string
     */
    public function filter($value)
    {
        $float = new Float();
        $value = $float->filter($value);
        return number_format($value, $this->decimals, $this->decimalPoint, $this->thousandsSeparator);
    }

    /**
     * @return string
     */
    public function getDecimalPoint()
    {
        return $this->decimalPoint;
    }

    /**
     * @param string $decimalPoint
     */
    public function setDecimalPoint($decimalPoint)
    {
        $this->decimalPoint = $decimalPoint;
    }

    /**
     * @return int
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * @param int $decimals
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;
    }

    /**
     * @return string
     */
    public function getThousandsSeparator()
    {
        return $this->thousandsSeparator;
    }

    /**
     * @param string $thousandsSeparator
     */
    public function setThousandsSeparator($thousandsSeparator)
    {
        $this->thousandsSeparator = $thousandsSeparator;
    }
} 