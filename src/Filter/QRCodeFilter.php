<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 5/23/14
 * Time: 1:12 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;


/**
 * Class QRCodeFilter
 * @package Filter\QRCode
 */
class QRCodeFilter extends AbstractFilter {

    /**
     * @var string
     */
    protected $destination;

    /**
     * @var int
     */
    protected $mode;

    /**
     * @var string
     */
    protected $filename;

    /**
     * @var int
     */
    protected $size = 4;

    /**
     * @var bool
     */
    protected $useValueAsFileName = false;

    /**
     * @var bool
     */
    protected $filterReturnPath = true;

    /**
     * Constructor
     * @param $options
     */
    public function __construct( $options = array() )
    {
        $this->setOptions($options);
    }

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws \Exception if filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        if ($this->getDestination() == null)
        {
            throw new \Exception('QRCode filter requires a destination to save the code!');
        }

        if ($this->useValueAsFileName)
        {
            $this->setFilename($value);
        }

        require_once('QrCode/qrlib.php');
        \QRcode::png($value, $this->getDestination() . $this->getFilename(), $this->getMode(), $this->getSize());

        if (!$this->getFilterReturnPath())
        {
            return $this->getDestination() . $this->getFilename();
        }

        $filter = new ImagePathName(array('keepPublicDirectory'=>true));
        return $filter->filter($this->getDestination() . $this->getFilename());
    }

    /**
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param string $destination
     */
    public function setDestination($destination)
    {
        if( substr($destination, -1) != '/')
        {
            $destination .= '/';
        }

        $this->destination = $destination;
    }

    /**
     * @return int
     */
    public function getMode()
    {
        if( $this->mode == null )
        {
            $this->mode = 0;
        }

        return $this->mode;
    }

    /**
     * @param int $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        if( $this->filename == null )
        {
            $this->filename = microtime(false) . '.png';
        }

        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        if( ! stristr($filename, '.png'))
        {
            $filename .= '.png';
        }

        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return boolean
     */
    public function getUseValueAsFileName()
    {
        return $this->useValueAsFileName;
    }

    /**
     * @param boolean $useValueAsFileName
     */
    public function setUseValueAsFileName($useValueAsFileName)
    {
        $this->useValueAsFileName = $useValueAsFileName;
    }

    /**
     * @return boolean
     */
    public function getFilterReturnPath()
    {
        return $this->filterReturnPath;
    }

    /**
     * @param boolean $filterReturnPath
     */
    public function setFilterReturnPath($filterReturnPath)
    {
        $this->filterReturnPath = $filterReturnPath;
    }
}