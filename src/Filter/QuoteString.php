<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda
 * Date: 4/29/16 12:04 PM
 *
 * (c) Copyright 2016 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;

class QuoteString extends AbstractFilter
{
    /**
     * @var string
     */
    protected $quote = '"';

    /**
     * @return string
     */
    public function getQuote()
    {
        return $this->quote;
    }

    /**
     * @param string $quote
     */
    public function setQuote($quote)
    {
        $this->quote = $quote;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function filter($value)
    {
        if( is_string($value) )
        {
            return $this->getQuote() . addslashes($value) . $this->getQuote();
        }

        return $value;
    }
}