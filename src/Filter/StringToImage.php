<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 2:56 PM
 * Project: filter
 */
namespace Filter;
use Zend\Filter\AbstractFilter;
use Zend\Validator\Uri;

/**
 * Class StringToImage
 * @package Filter
 */
class StringToImage extends AbstractFilter {

    /**
     * Orientation Flag 6
     */
    const ROTATE_180 = 180;

    /**
     * Orientation Flag 3
     */
    const ROTATE_90_CCW = 270;

    /**
     * Orientation Flag 8
     */
    const ROTATE_90_CW = 90;

    /**
     * Directory to store the images in
     * @var string
     */
    protected $target;

    /**
     * Flag for orientation correction
     * @var bool
     */
    protected $correctOrientation;

    /**
     * @var boolean
     */
    protected $randomize;

    /**
     * The maximum allowed size
     * @var array
     */
    protected $maximumSize;

    /**
     * The Resize Method
     * @var string
     */
    protected $resizeMethod = 'thumb';

    /**
     * The image quality
     * This is really a speed vs file size issue
     * 0 = fastest, but largest file size
     * 9 = slowest, but smallest file size
     * @var int
     */
    protected $quality = 9;

    /**
     * A string to replace 'public/' with
     * @var string
     */
    protected $replacePublic = false;

    /**
     * Flag for whether the directory should be automatically created
     * @var bool
     */
    protected $autoCreateTargetDirectory = false;

    /**
     * A size to pad the image to
     * @var null
     */
    protected $padToSize = null;

    /**
     * The color to pad to
     * @var array
     */
    protected $padToColor = array('255','255','255');

    /**
     * Size up to max size flag
     * @var bool
     */
    protected $sizeUp = false;

    /**
     * Stores the images processed on this request.
     * We do this because if the filter is consumed by an input filter,
     * the input filter may filter the data more than once, creating multiple images
     * of the same content
     * @var array
     */
    protected static $processedImages = array();

    /**
     * When true, forces a render even if the string is in $processedImages
     * @var bool
     */
    protected $forceRenderEvenIfPreviouslyProcessed = false;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @return array
     */
    public static function getProcessedImages()
    {
        return self::$processedImages;
    }

    /**
     * @param array $processedImages
     */
    public static function setProcessedImages($processedImages)
    {
        self::$processedImages = $processedImages;
    }

    /**
     * @return string
     */
    public function getReplacePublic()
    {
        return $this->replacePublic;
    }

    /**
     * @param string $replacePublic
     */
    public function setReplacePublic($replacePublic)
    {
        $this->replacePublic = $replacePublic;
    }

    /**
     * @return mixed
     */
    public function getCorrectOrientation()
    {
        return $this->correctOrientation;
    }

    /**
     * @param mixed $correctOrientation
     */
    public function setCorrectOrientation($correctOrientation)
    {
        $this->correctOrientation = $correctOrientation;
    }

    /**
     * @return int
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    }

    /**
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @param string $target
     */
    public function setTarget($target)
    {
        $target = str_ireplace(array('/', '\\'), DIRECTORY_SEPARATOR, $target);

        if( substr($target, -1) != DIRECTORY_SEPARATOR )
        {
            $target .= DIRECTORY_SEPARATOR;
        }

        $this->target = $target;
    }

    /**
     * @return boolean
     */
    public function isRandomize()
    {
        return $this->randomize;
    }

    /**
     * @param boolean $randomize
     */
    public function setRandomize($randomize)
    {
        $this->randomize = $randomize;
    }

    /**
     * @return array
     */
    public function getMaximumSize()
    {
        return $this->maximumSize;
    }

    /**
     * @param array|string $maximumSize
     * @throws \Exception
     */
    public function setMaximumSize($maximumSize)
    {
        if( is_string($maximumSize) )
        {
            $maximumSize = explode('x', strtolower($maximumSize));

            if( $maximumSize === false || count($maximumSize) == 1 )
            {
                throw new \Exception('Maximum size couldn\'t be converted to an array from string!');
            }

            $maximumSize = array(
                'width'=>(int)trim($maximumSize[0]),
                'height'=>(int)trim($maximumSize[1])
            );
        }

        if( ! is_array($maximumSize) )
        {
            throw new \Exception('Maximum size must be an array or a string of dimensions like 250x500');
        }

        $this->maximumSize = $maximumSize;
    }

    /**
     * @return boolean
     */
    public function getAutoCreateTargetDirectory()
    {
        return $this->autoCreateTargetDirectory;
    }

    /**
     * @param boolean $autoCreateTargetDirectory
     */
    public function setAutoCreateTargetDirectory($autoCreateTargetDirectory)
    {
        $this->autoCreateTargetDirectory = $autoCreateTargetDirectory;
    }


    /**
     * @return string
     */
    public function getResizeMethod()
    {
        return $this->resizeMethod;
    }

    /**
     * @param string $resizeMethod
     */
    public function setResizeMethod($resizeMethod)
    {
        $this->resizeMethod = $resizeMethod;
    }

    /**
     * @return array
     */
    public function getPadToColor()
    {
        return $this->padToColor;
    }

    /**
     * @param array $padToColor
     */
    public function setPadToColor($padToColor)
    {
        if( is_string($padToColor) )
        {
            $padToColor = explode(',', $padToColor);
        }

        $this->padToColor = $padToColor;
    }

    /**
     * @return null
     */
    public function getPadToSize()
    {
        return $this->padToSize;
    }

    /**
     * @param null $padToSize
     */
    public function setPadToSize($padToSize)
    {
        if( is_string($padToSize) )
        {
            $padToSize = explode('x', $padToSize);
        }

        $this->padToSize = $padToSize;
    }

    /**
     * @return bool
     */
    public function getSizeUp()
    {
        return $this->sizeUp;
    }

    /**
     * @param bool $sizeUp
     */
    public function setSizeUp($sizeUp)
    {
        $this->sizeUp = $sizeUp;
    }

    /**
     * @return bool
     */
    public function getForceRenderEvenIfPreviouslyProcessed()
    {
        return $this->forceRenderEvenIfPreviouslyProcessed;
    }

    /**
     * @param bool $forceRenderEvenIfPreviouslyProcessed
     */
    public function setForceRenderEvenIfPreviouslyProcessed($forceRenderEvenIfPreviouslyProcessed)
    {
        $this->forceRenderEvenIfPreviouslyProcessed = $forceRenderEvenIfPreviouslyProcessed;
    }

    /**
     * Gets the options
     * @return array
     */
    public function getOptions()
    {
        $base =  parent::getOptions();

        if( ! is_array($base))
        {
            $base = array();
        }

        $options = [
            'force_render_even_if_previously_processed'=>$this->getForceRenderEvenIfPreviouslyProcessed(),
            'target'=>$this->getTarget(),
            'auto_create_target_directory'=>$this->getAutoCreateTargetDirectory(),
            'correct_orientation'=>$this->getCorrectOrientation(),
            'maximum_size'=>$this->getMaximumSize(),
            'pad_to_color'=>$this->getPadToColor(),
            'pad_to_size'=>$this->getPadToSize(),
            'quality'=>$this->getQuality(),
            'replace_public'=>$this->getReplacePublic(),
            'resize_method'=>$this->getResizeMethod(),
            'size_up'=>$this->getSizeUp(),
        ];

        return array_merge($base, $options);
    }


    /**
     * Receives posted images as raw data and makes them image files
     * @param mixed $imageData
     * @throws \Exception
     * @return mixed|string
     */
    public function filter($imageData)
    {
        if( $imageData == null || $imageData == '' )
        {
            return $imageData;
        }

        if( is_string($imageData) )
        {
            return $this->processImage($imageData);
        }
        elseif( is_array($imageData) )
        {
            $filenames = array();

            foreach($imageData as $realImageData)
            {
                $filenames[] = $this->processImage($realImageData);
            }

            return $filenames;
        }

        throw new \Exception('Image Data was not a string or an array');
    }

    /**
     * Processes the string to an image file
     * @param $imageData
     * @throws \Exception
     * @return string
     */
    protected function processImage($imageData)
    {
        $uri = new Uri(array('allowRelative'=>true, 'allowAbsolute'=>true));

        if( $uri->isValid($imageData) && ! stristr($imageData, 'data:image')) // if it is a uri, we are probably passing in a link to an image
        {
            return $imageData;
        }

        // if it was already processed and we are not forcing a rerender, return the image path we already made
        if( ! $this->getForceRenderEvenIfPreviouslyProcessed() )
        {
            if( $key = array_search($imageData, self::$processedImages) )
            {
                return $key;
            }
        }

        if( $this->getTarget() == null )
        {
            throw new \Exception('Source Directory not set');
        }

        if( $this->autoCreateTargetDirectory )
        {
            if( ! is_dir($this->getTarget()) && ! mkdir($this->getTarget(), 0777, true) )
            {
                throw new \Exception('Could not automatically create directory: ' . $this->getTarget());
            }
        }

        $replaces = array(
            'data:image/jpeg;base64,',
            'data:image/jpg;base64,',
            'data:image/gif;base64,',
            'data:image/png;base64,',
        );

        $imageType = null;

        if( stristr($imageData, 'image/jpeg') || stristr($imageData, 'image/jpg'))
        {
            $imageType = 'jpg';
        }
        elseif( stristr($imageData, 'image/png') )
        {
            $imageType = 'png';
        }
        elseif( stristr($imageData, 'image/gif'))
        {
            $imageType = 'gif';
        }

        if( $imageType == null )
        {
            throw new \Exception('Could not detect image type!');
        }

        $imageString = base64_decode(str_ireplace($replaces, '', $imageData));

        $rotation = false;
        $microtime = microtime(false) . uniqid();

        // Do the orientation detection prior to making the image
        if( $this->getCorrectOrientation() )
        {
            $filename = str_replace(array('.', ' '), '', $microtime) . '.jpg';
            $temporaryTarget = $this->getTarget() . $filename;

            $tempFileHandler = fopen($temporaryTarget, 'w+');
            fwrite($tempFileHandler, $imageString);
            fclose($tempFileHandler);

            $metaData = exif_read_data($temporaryTarget);
            unlink($temporaryTarget);

            if (is_array($metaData) && isset($metaData['Orientation']) && $metaData['Orientation'] != 1)
            {
                $rotation = $metaData['Orientation'];
            }
        }

        $result = imagecreatefromstring($imageString);

        if ($result !== false)
        {
            $filename = str_replace(array('.', ' '), '', $microtime) . '.' . $imageType;
            $target = $this->getTarget() . $filename;

            if( $imageType == 'png' )
            {
                imagealphablending($result, true);
            }

            if ($rotation)
            {
                switch ($rotation)
                {
                    case 3:
                    {
                        $result = imagerotate($result, self::ROTATE_180, 0);
                        break;
                    }
                    case 8:
                    {
                        $result = imagerotate($result, self::ROTATE_90_CW, 0);
                        break;
                    }
                    case 6:
                    {
                        $result = imagerotate($result, self::ROTATE_90_CCW, 0);
                        break;
                    }
                }
            }

            if( $imageType == 'png' )
            {
                imagealphablending( $result, false );
                imagesavealpha( $result, true );
                $imageResult = imagepng($result, $target, $this->getQuality(), PNG_ALL_FILTERS );
            }
            elseif( $imageType == 'jpg' )
            {
                $imageResult = imagejpeg($result, $target, $this->getQuality() * 11);
            }
            elseif( $imageType == 'gif' )
            {
                $imageResult = imagegif($result, $target);
            }

            imagedestroy($result);

            if( $imageResult == false || $imageResult == null )
            {
                throw new \Exception('An error occurred while trying to write the image at ' . $target);
            }

            chmod($target, 0777);

            if( is_array($this->getMaximumSize()) )
            {
                $config = $this->getMaximumSize();

                if( ! isset($config['width']) || ! isset($config['height']))
                {
                    throw new \Exception('Width or Height not set');
                }

                $thumbnailer = new Image();
                $image = $thumbnailer->create($target);

                if( $this->getSizeUp() )
                {
                    $image->setOptions(array('resizeUp'=>true));
                }

                if( $this->getResizeMethod() == 'crop' )
                {
                    $image->adaptiveResize($config['width'], $config['height']);
                }
                else
                {
                    $image->resize($config['width'], $config['height']);
                }

                if( $this->getPadToSize() != null )
                {
                    $pad = $this->getPadToSize();
                    $image->pad($pad[0], $pad[1], $this->getPadToColor());
                }

                $image->save($target);
                $image = null; // Memory Clean up
                $thumbnailer = null; // Memory Clean up
            }

            if( $this->getReplacePublic() === false )
            {
                self::$processedImages[$target] = $imageData;
                return $target;
            }

            $target = str_ireplace(array('public/', ' '), array($this->getReplacePublic(), ''), $target);
            self::$processedImages[$target] = $imageData;
            return $target;
        }
        else
        {
            throw new \Exception('An error occurred while trying to create an image from a string');
        }
    }
}
