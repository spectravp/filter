<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda 
 * Date: 12/4/14 12:36 PM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */

namespace Filter\ViewHelper;


use Zend\View\Helper\AbstractHelper;

/**
 * Class Filter
 * @package Filter\ViewHelper
 */
class Filter extends AbstractHelper {

    /**
     * Proxies to a filter in the filter manager
     * @param $filterName
     * @param $value
     * @param array $options
     * @return mixed
     * @throws \Exception
     */
    public function __invoke($filterName, $value, $options = array())
    {
        $serviceLocator = $this->getView()->getHelperPluginManager()->getServiceLocator();
        $filterManager = $serviceLocator->get('filterManager');

        if( ! stristr($filterName, 'Filter') )
        {
            if( $filterManager->has('Filter\\' . $filterName) )
            {
                $filterName = 'Filter\\' . $filterName;
            }
            elseif( $filterManager->has('Zend\\Filter\\' . $filterName))
            {
                $filterName = 'Zend\\Filter\\' . $filterName;
            }
        }

        if( ! $filterManager->has($filterName) )
        {
            throw new \Exception("No filter by the name of $filterName could be retrieve from the Filter Manager");
        }

        $filter = $filterManager->get($filterName);
        $filter->setOptions($options);
        return $filter->filter($value);
    }
} 