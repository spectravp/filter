<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda
 * Date: 6/27/16 1:43 PM
 *
 * (c) Copyright 2016 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class PhpTimestampToJs extends AbstractFilter
{
    public function filter($value)
    {
        return $value * 1000;
    }
}