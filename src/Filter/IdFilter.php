<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 8/12/14
 * Time: 11:15 AM
 * Project: filter
 */
namespace Filter;
use Zend\Filter\Digits;
use Zend\Filter\AbstractFilter;

/**
 * Class IdFilter
 * @package Filter
 */
class IdFilter extends AbstractFilter {

    /**
     * @var bool
     */
    protected $crypt = false;

    /**
     * @var string
     */
    protected $cryptMethod = self::ENCRYPT;

    /**
     * Flag to Encrypt Id
     */
    const ENCRYPT = '\\Filter\\TinyCrypt\\Encrypt';

    /**
     * Flag to Decrypt Id
     */
    const DECRYPT = '\\Filter\\TinyCrypt\\Decrypt';

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $value
     * @return int|string
     */
    public function filter($value)
    {
        $digits = new Digits();

        if( $this->getCrypt() === true )
        {
            $crypt = new $this->getCryptMethod();

            if( $this->getCryptMethod() == self::ENCRYPT )
            {
                return $crypt->filter($digits->filter($value));
            }

            return $digits->filter($crypt->filter($value));
        }

        return $digits->filter($value);
    }

    /**
     * @return boolean
     */
    public function getCrypt()
    {
        return $this->crypt;
    }

    /**
     * @param boolean $crypt
     */
    public function setCrypt($crypt)
    {
        $this->crypt = $crypt;
    }

    /**
     * @return string
     */
    public function getCryptMethod()
    {
        return $this->cryptMethod;
    }

    /**
     * @param string $cryptMethod
     */
    public function setCryptMethod($cryptMethod)
    {
        $this->cryptMethod = $cryptMethod;
    }
} 