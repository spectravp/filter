<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/21/13
 * Time: 7:11 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class UpperCaseWords
 * @package Filter
 */
class UpperCaseWords extends AbstractFilter {

    /**
     * @var bool
     */
    protected $stringToLower = false;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Makes the words uppercase
     * @param mixed $value
     * @return mixed|string
     */
    public function filter($value)
    {
        if( $this->getStringToLower() )
        {
            $value = strtolower($value);
        }

        return ucwords($value);
    }

    /**
     * @return boolean
     */
    public function getStringToLower()
    {
        return $this->stringToLower;
    }

    /**
     * @param boolean $stringToLower
     */
    public function setStringToLower($stringToLower)
    {
        $this->stringToLower = $stringToLower;
    }
} 