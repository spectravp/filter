<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda 
 * Date: 12/4/14 12:28 PM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;

/**
 * Class Utf8
 * @package Filter
 */
class Utf8 extends AbstractFilter {

    /**
     * Flag to use MB String extension
     * @var bool
     */
    protected $useMbString = true;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * Converts a string to UTF-8 character encoding
     * @param mixed $value
     * @return mixed|string
     */
    public function filter($value)
    {
        if( is_string($value) )
        {
            if( function_exists('mb_convert_encoding') && $this->getUseMbString() )
            {
                return mb_convert_encoding($value, 'UTF-8');
            }

            return utf8_encode($value);
        }

        return $value;
    }

    /**
     * @return boolean
     */
    public function getUseMbString()
    {
        return $this->useMbString;
    }

    /**
     * @param boolean $useMbString
     */
    public function setUseMbString($useMbString)
    {
        $this->useMbString = $useMbString;
    }
} 