<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda
 * Date: 6/23/15 1:27 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

class BooleanizeJsonString extends AbstractFilter
{
    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Exception\RuntimeException If filtering $value is impossible
     * @return mixed
     */
    public function filter($value)
    {
        return str_ireplace(array("'", '"false"', '"true"', "'false'", "'true'"), array('"', 'false', 'true','false', 'true'), $value);
    }
}