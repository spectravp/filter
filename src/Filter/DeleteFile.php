<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 8/9/15
 * Time: 3:13 PM
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

/**
 * Class DeleteFile
 * @package Filter
 */
class DeleteFile extends AbstractFilter
{
    /**
     * @var bool
     */
    protected $throwExceptions = false;

    /**
     * @return boolean
     */
    public function getThrowExceptions()
    {
        return $this->throwExceptions;
    }

    /**
     * @param boolean $throwExceptions
     */
    public function setThrowExceptions($throwExceptions)
    {
        $this->throwExceptions = $throwExceptions;
    }

    /**
     * @param mixed $value
     * @throws \Exception
     * @return bool
     */
    public function filter($value)
    {
        $result = unlink($value);

        if( ! $result && $this->getThrowExceptions() )
        {
            throw new \Exception('Could not unlink file: ' . $value);
        }

        return $result;
    }
}