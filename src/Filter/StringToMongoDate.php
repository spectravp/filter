<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/10/15
 * Time: 6:27 PM
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

/**
 * Class StringToMongoDate
 * @package Filter
 */
class StringToMongoDate extends AbstractFilter
{

    /**
     * @param mixed $date
     * @return mixed|\MongoDate
     * @throws \Exception
     */
    public function filter($date)
    {
        if( is_string($date) )
        {
            $dt = new \DateTime($date);
            $dt->setTimezone(new \DateTimeZone('UTC'));
            $date = new \MongoDate($dt->getTimestamp());
        }
        else if ($date instanceof \DateTime)
        {
            $date->setTimezone(new \DateTimeZone('UTC'));
            $date = new \MongoDate($date->getTimestamp());
        }
        else
        {
            $date = null;
        }

        return $date;
    }
}