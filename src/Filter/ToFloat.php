<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/30/13
 * Time: 4:03 PM
 */
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Float
 * @package Filter
 */
class ToFloat extends AbstractFilter {

    /**
     * @var int
     */
    protected $decimals = 2;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $value
     * @return float
     */
    public function filter($value)
    {
        if( is_string($value) )
        {
            $value = (float)preg_replace("/[^-0-9\.]/i","", $value);
        }
        
        return (float)number_format(round($value, $this->getDecimals()), $this->getDecimals(), '.', '');
    }

    /**
     * @return int
     */
    public function getDecimals()
    {
        return $this->decimals;
    }

    /**
     * @param int $decimals
     */
    public function setDecimals($decimals)
    {
        $this->decimals = $decimals;
    }
}