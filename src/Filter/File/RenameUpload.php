<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda
 * Date: 12/23/15 2:41 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */

namespace Filter\File;

use \Zend\Filter\File\RenameUpload as ZendFilter;
use Zend\Stdlib\ErrorHandler;
use Zend\Filter\Exception;
class RenameUpload extends ZendFilter
{
    /**
     * @var bool
     */
    protected $stripPublic = true;

    /**
     * Flag for automatic creation of destination directory
     * @var bool
     */
    protected $autoCreateDirectory = false;

    /**
     * @return boolean
     */
    public function getStripPublic()
    {
        return $this->stripPublic;
    }

    /**
     * @param boolean $stripPublic
     */
    public function setStripPublic($stripPublic)
    {
        $this->stripPublic = $stripPublic;
    }

    /**
     * @return boolean
     */
    public function getAutoCreateDirectory()
    {
        return $this->autoCreateDirectory;
    }

    /**
     * @param boolean $autoCreateDirectory
     */
    public function setAutoCreateDirectory($autoCreateDirectory)
    {
        $this->autoCreateDirectory = $autoCreateDirectory;
    }

    /**
     * Defined by Zend\Filter\Filter
     *
     * Renames the file $value to the new name set before
     * Returns the file $value, removing all but digit characters
     *
     * @param  string|array $value Full path of file to change or $_FILES data array
     * @throws Exception\RuntimeException
     * @return string|array The new filename which has been set, or false when there were errors
     */
    public function filter($value)
    {
        $data = parent::filter($value);

        if( is_array($data) && isset($data['tmp_name']))
        {
            if( $this->stripPublic )
            {
                return str_ireplace('public/', '/', $data['tmp_name']);
            }

            return $data['tmp_name'];
        }

        return $data;
    }

    /**
     * @param  string $sourceFile Source file path
     * @param  string $targetFile Target file path
     * @throws Exception\RuntimeException
     * @return bool
     */
    protected function moveUploadedFile($sourceFile, $targetFile)
    {
        if( $this->getAutoCreateDirectory() )
        {
            $path = explode('/', $targetFile);
            array_pop($path);
            $path = join('/', $path);
            mkdir($path, 0777, true);
        }

        ErrorHandler::start();
        $result = rename($sourceFile, $targetFile);
        $warningException = ErrorHandler::stop();
        if (!$result || null !== $warningException) {

            throw new Exception\RuntimeException(
                sprintf("File '%s' could not be renamed. An error occurred while processing the file.", $sourceFile),
                0,
                $warningException
            );
        }

        return $result;
    }
}