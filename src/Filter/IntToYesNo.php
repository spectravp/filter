<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/11/13
 * Time: 9:49 PM
 */

namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class IntToYesNo
 * @package Filter
 */
class IntToYesNo extends YesNo {

    public function __construct($options = array())
    {
        trigger_error('Filter\IntToYesNo is deprecated because it supports more than ints. Use Filter\YesNo', E_DEPRECATED);
        parent::__construct($options);
    }
} 