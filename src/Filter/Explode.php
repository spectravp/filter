<?php
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Image
 * @package Filter
 */
class Explode extends AbstractFilter {

    /**
     * @var string
     */
    protected $delimiter = ',';

    /**
     * @return string
     */
    public function getDelimiter()
    {
        return $this->delimiter;
    }

    /**
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->delimiter = $delimiter;
    }

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $value
     * @return float
     */
    public function filter($value)
    {
        if( is_string($value))
        {
            return explode($this->getDelimiter(), $value);
        }

        return $value;
    }

}