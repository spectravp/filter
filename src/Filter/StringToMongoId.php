<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/10/15
 * Time: 6:27 PM
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

/**
 * Class MongoId
 * @package Filter
 */
class StringToMongoId extends AbstractFilter
{
    /**
     * @param mixed $id
     * @return \MongoId
     * @throws \Exception
     */
    public function filter($id)
    {
        if( is_string($id) )
        {
            // Older versions of mongo don't support the isValid() method. Test first.
            if( method_exists(new \MongoId(), 'isValid'))
            {
                if (!\MongoId::isValid($id))
                {
                    throw new \Exception('Invalid Mongo ID string: ' . $id);
                }
            }

            $id = new \MongoId($id);
        }

        if( ! $id instanceof \MongoId )
        {
            throw new \Exception('Could not create a Mongo Id');
        }

        return $id;
    }
}