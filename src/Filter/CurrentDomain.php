<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda
 * Date: 12/21/15 3:51 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;

class CurrentDomain extends AbstractFilter
{
    /**
     * @var string
     */
    protected $replacement = '';

    /**
     * @var array
     */
    protected $protocols = array(
        'http://',
        'https://'
    );

    /**
     * @var array
     */
    protected $subDomains = array(
        'www'
    );

    /**
     * @return array
     */
    public function getProtocols()
    {
        return $this->protocols;
    }

    /**
     * @param array $protocols
     */
    public function setProtocols($protocols)
    {
        $this->protocols = $protocols;
    }

    /**
     * @param $protocol
     */
    public function addProtocol($protocol)
    {
        $this->protocols[] = $protocol;
    }

    /**
     * @return array
     */
    public function getSubDomains()
    {
        return $this->subDomains;
    }

    /**
     * @param array $subDomains
     */
    public function setSubDomains($subDomains)
    {
        $this->subDomains = $subDomains;
    }

    /**
     * @param $subDomain
     */
    public function addSubDomain($subDomain)
    {
        $this->subDomains[] = $subDomain;
    }

    /**
     * @return string
     */
    public function getReplacement()
    {
        return $this->replacement;
    }

    /**
     * @param string $replacement
     */
    public function setReplacement($replacement)
    {
        $this->replacement = $replacement;
    }



    /**
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        $host = $_SERVER['HTTP_HOST'];

        foreach($this->getSubDomains() as $subDomain)
        {
            if (stristr($host, $subDomain . '.')) {
                $host = str_ireplace($subDomain . '.', '', $host);
            }
        }

        $finds = array();

        foreach($this->getProtocols() as $protocol)
        {
            foreach($this->getSubDomains() as $subDomain)
            {
                $finds[] = $protocol . $subDomain . '.' . $host;
            }

            $finds[] = $protocol . $host;
        }

        foreach($this->getSubDomains() as $subDomain)
        {
            $finds[] = $subDomain . '.' . $host;
        }

        $finds[] = $host;

        return str_ireplace($finds, $this->replacement, $value);
    }
}