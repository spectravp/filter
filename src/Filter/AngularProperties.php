<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/10/15
 * Time: 6:27 PM
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

/**
 * Class MongoId
 * @package Filter
 */
class AngularProperties extends AbstractFilter
{
    /**
     * An array of object/array properties to strip
     * @var array
     */
    protected $invalidProperties = array(
        '$$hashKey',
        '$$ChildScope',
        '$$childHead',
        '$$childTail',
        '$$listenerCount',
        '$$listeners',
        '$$nextSibling',
        '$$prevSibling',
        '$$wacthers',
        '$parent',
        '$$applyAsyncQueue',
        '$$destroyed',
        '$$isolateBindings',
        '$$phase',
        '$$postDigestQueue',
        '$root',
        '$scope'
    );

    /**
     * Filters out keys that angular could have potentially added to
     * the array/object. This can cause database problems.
     * @param mixed $arrayOrObject
     * @return mixed
     */
    public function filter($arrayOrObject)
    {
        if(is_array($arrayOrObject) || is_object($arrayOrObject))
        {
            foreach($arrayOrObject as $key => $value)
            {
                // If they key is in the array,
                // kill it regardless of the type of object it is
                if( ! is_int($key) && in_array($key, $this->invalidProperties))
                {
                    if( is_array($value) )
                    {
                        unset($arrayOrObject[$key]);
                    }
                    elseif( is_object($value))
                    {
                        unset($arrayOrObject->$key);
                    }
                }

                // the key is valid and if its an array or object
                // we need to look deeper into it...
                if( is_array($value) || is_object($value))
                {
                    $this->filter($value);
                }
                else
                {
                    // its not an array or object, so test it.
                    if( in_array($key, $this->invalidProperties))
                    {
                        if( is_array($arrayOrObject) )
                        {
                            unset($arrayOrObject[$key]);
                        }
                        elseif( is_object($arrayOrObject))
                        {
                            unset($arrayOrObject->$key);
                        }
                    }
                }
            }
        }

        return $arrayOrObject;
    }
}