<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 8/9/15
 * Time: 3:13 PM
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\Exception;

/**
 * Class DeleteFile
 * @package Filter
 */
class Pluralize extends AbstractFilter
{
    /**
     * @param mixed $value
     * @throws \Exception
     * @return bool
     */
    public function filter($value)
    {
        if (substr($value, strlen($value) - 1, 1) != 's')
        {
            return $value . 's';
        }
    }
}