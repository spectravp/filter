<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/30/13
 * Time: 4:03 PM
 */
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Float
 * @package Filter
 */
class ToInt extends AbstractFilter {

    /**
     * @param $value
     * @return float
     */
    public function filter($value)
    {
        if (!is_scalar($value)) {
            return $value;
        }
        $value = (string) $value;

        return (int) $value;
    }
}