<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 11/30/13
 * Time: 4:03 PM
 */
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Float
 * @package Filter
 */
class ToString extends AbstractFilter {

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $value
     * @return float
     */
    public function filter($value)
    {
        return (string)$value;
    }
}