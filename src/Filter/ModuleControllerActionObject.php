<?php


namespace Filter;
use Zend\Filter\AbstractFilter;
use Zend\Filter\Word\CamelCaseToDash;

/**
 * Class ModuleControllerActionObject
 * @package Filter
 */
class ModuleControllerActionObject extends AbstractFilter {

    /**
     * @var bool
     */
    protected $strToLower = false;

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        if( is_bool($options) )
        {
            $options = array('str_to_lower'=>$options);
        }

        $this->setOptions($options);
    }

    /**
     * Filters a route (module/controller/action) to a stdClass object
     * @param mixed $route
     * @return mixed|\stdClass
     * @throws \Exception
     */
    public function filter($route)
    {
        if(! is_array($route) )
        {
            throw new \Exception('Filter ' . __CLASS__ . ' expects parameter to be an array from RouteMatch()->getParams()');
        }

        $controllerFilter = new CamelCaseToDash();

        $parts = explode('\\', $route['controller']);
        $obj = new \stdClass();
        $obj->module = $parts[0];
        $obj->controller = $controllerFilter->filter($parts[2]);
        $obj->action = @$route['action'];

        if( $this->strToLower )
        {
            $obj->controller = strtolower($obj->controller);
            $obj->action = strtolower($obj->action);
        }

        return $obj;
    }

    /**
     * @return boolean
     */
    public function isStrToLower()
    {
        return $this->strToLower;
    }

    /**
     * @param boolean $strToLower
     */
    public function setStrToLower($strToLower)
    {
        $this->strToLower = $strToLower;
    }
} 