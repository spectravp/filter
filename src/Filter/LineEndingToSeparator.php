<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda 
 * Date: 12/8/14 3:47 PM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;

/**
 * Class LineEndingToSeparator
 * @package Filter
 */
class LineEndingToSeparator extends AbstractFilter {


    /**
     * @var string
     */
    protected $separator = '<br>';

    /**
     * Tag to wrap all the contents with
     * @var null
     */
    protected $wrap = null;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        $string = str_replace(PHP_EOL, $this->getSeparator(), $value);

        if( $this->getWrap() !== null )
        {
            $string = '<'.$this->getWrap().'>'.$string.'</'.$this->getWrap().'>';
        }

        return $string;
    }

    /**
     * @return string
     */
    public function getSeparator()
    {
        return $this->separator;
    }

    /**
     * @param string $separator
     */
    public function setSeparator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @return null
     */
    public function getWrap()
    {
        return $this->wrap;
    }

    /**
     * @param null $wrap
     */
    public function setWrap($wrap)
    {
        $this->wrap = $wrap;
    }
} 