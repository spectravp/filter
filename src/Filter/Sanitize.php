<?php
/**
 * ZDI Design Group
 * Project filter
 * Author derekmiranda 
 * Date: 1/6/15 1:33 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */

namespace Filter;


use Zend\Filter\AbstractFilter;
use Zend\Filter\FilterChain;

/**
 * Class Sanitize
 * @package Filter
 * @description Proxies to a filter chain to filter data in one swoop, rather than have to add 3 filters each time
 */
class Sanitize extends AbstractFilter {

    /**
     * @var FilterChain
     */
    protected $filterChain;

    /**
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    public function filter($data)
    {
        return $this->getFilterChain()->filter($data);
    }

    /**
     * @return FilterChain
     */
    protected function getFilterChain()
    {
        if($this->filterChain == null )
        {
            $this->filterChain = new FilterChain();
            $this->filterChain->attachByName('StripNewLines');
            $this->filterChain->attachByName('StripTags');
            $this->filterChain->attachByName('StringTrim');
        }

        return $this->filterChain;
    }
}