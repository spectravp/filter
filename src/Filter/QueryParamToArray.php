<?php
/**
 * Created by zdi design group
 * http://www.zdidesigngroup.com
 *
 * User: derekmiranda
 * Date: 7/31/14
 * Time: 11:20 AM
 * Project: social-chinese-web
 */
namespace Filter;
use Zend\Filter\AbstractFilter;

/**
 * Class QueryParamToArray
 * @package Filter
 */
class QueryParamToArray extends SeparatorToArray {

    /**
     * @deprecated
     * @param array $options
     */
    public function __construct($options = array())
    {
        trigger_error('QueryParamToArray is deprecated. See SeparatorToArray', E_DEPRECATED);
        $this->setOptions($options);
    }
}