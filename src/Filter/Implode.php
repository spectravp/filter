<?php
namespace Filter;

use Zend\Filter\AbstractFilter;

/**
 * Class Image
 * @package Filter
 */
class Implode extends AbstractFilter {

    /**
     * @var string
     */
    protected $glue = ', ';

    /**
     * @return string
     */
    public function getGlue()
    {
        return $this->glue;
    }

    /**
     * @param string $glue
     */
    public function setGlue($glue)
    {
        $this->glue = $glue;
    }

    /**
     * Constructor
     * @param array $options
     */
    public function __construct($options = array())
    {
        $this->setOptions($options);
    }

    /**
     * @param $value
     * @return float
     */
    public function filter($value)
    {
        if( is_array($value))
        {
            return implode($this->getGlue(), $value);
        }

        return $value;
    }
}