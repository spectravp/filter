<?php
use Filter\IntToBoolean;


/**
 * ZDI Design Group
 * Project validator
 * Author derekmiranda 
 * Date: 12/5/14 3:00 PM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */
class IntToBooleanTest extends PHPUnit_Framework_TestCase
{
    public function testIntToBool()
    {
        $filter = new IntToBoolean();
        $this->assertEquals(true, $filter->filter(true));
        $this->assertEquals(false, $filter->filter(false));
        $this->assertEquals(true, $filter->filter(1));
        $this->assertEquals(true, $filter->filter(rand(1,5000)));
        $this->assertEquals(false, $filter->filter(0));
        $this->assertEquals(true, $filter->filter('1'));
        $this->assertEquals(false, $filter->filter('0'));
    }
}