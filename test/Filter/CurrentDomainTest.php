<?php

use Filter\CurrentDomain;
use Filter\YesNo;

/**
 * ZDI Design Group
 * Project validator
 * Author derekmiranda 
 * Date: 12/5/14 3:00 PM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */
class CurrentDomainTest extends PHPUnit_Framework_TestCase
{
    public function testShortDomain()
    {
        $filter = new CurrentDomain();

        $_SERVER['HTTP_HOST'] = 'fred.com';
        $this->assertEquals('/test', $filter->filter('http://www.fred.com/test'));
        $this->assertEquals('/test2', $filter->filter('www.fred.com/test2'));
        $this->assertEquals('/test3', $filter->filter('https://www.fred.com/test3'));
    }

    public function testSubDomain()
    {
        $filter = new CurrentDomain();

        $_SERVER['HTTP_HOST'] = 'www.fred.com';
        $this->assertEquals('/test', $filter->filter('http://www.fred.com/test'));
        $this->assertEquals('/test2', $filter->filter('www.fred.com/test2'));
        $this->assertEquals('/test3', $filter->filter('https://www.fred.com/test3'));
    }

    public function testCustomSubDomain()
    {
        $filter = new CurrentDomain();
        $filter->addSubDomain('custom');

        $_SERVER['HTTP_HOST'] = 'custom.fred.com';
        $this->assertEquals('/test', $filter->filter('http://custom.fred.com/test'));
        $this->assertEquals('/test2', $filter->filter('custom.fred.com/test2'));
        $this->assertEquals('/test3', $filter->filter('https://custom.fred.com/test3'));

    }

    public function testReplacement()
    {
        $filter = new CurrentDomain();
        $filter->setReplacement('bigbob');

        $_SERVER['HTTP_HOST'] = 'www.fred.com';
        $this->assertEquals('bigbob/test', $filter->filter('http://www.fred.com/test'));
        $this->assertEquals('bigbob/test2', $filter->filter('www.fred.com/test2'));
        $this->assertEquals('bigbob/test3', $filter->filter('https://www.fred.com/test3'));

    }
}