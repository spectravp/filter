<?php

use Filter\YesNo;

/**
 * ZDI Design Group
 * Project validator
 * Author derekmiranda 
 * Date: 12/5/14 3:00 PM
 *
 * (c) Copyright 2014 derekmiranda | All Rights Reserved
 */
class YesNoTest extends PHPUnit_Framework_TestCase
{
    /**
     * Hex Color Test
     */
    public function testYesNo()
    {
        $filter = new YesNo();
        $this->assertEquals('Yes',$filter->filter(1));
        $this->assertEquals('Yes',$filter->filter(true));
        $this->assertEquals('Yes',$filter->filter(2));
        $this->assertEquals('Yes',$filter->filter('1'));

        $this->assertEquals('No', $filter->filter(null));
        $this->assertEquals('No', $filter->filter(0));
        $this->assertEquals('No', $filter->filter(false));
    }
}