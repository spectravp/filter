<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 2/27/14
 * Time: 3:04 PM
 */
return array(
    /**
     * Filter Plugin Manager Config
     */
    'filters'=>array(
        'invokables'=>array(
            'Filter\DefaultValue'                =>'Filter\DefaultValue',
            'Filter\AngularProperties'           =>'Filter\AngularProperties',
            'Filter\BooleanizeJsonString'        =>'Filter\BooleanizeJsonString',
            'Filter\BooleanToInteger'            =>'Filter\BooleanToInteger',
            'Filter\Currency'                    =>'Filter\Currency',
            'Filter\CurrentDomain'               =>'Filter\CurrentDomain',
            'Filter\DatabaseTableName'           =>'Filter\DatabaseTableName',
            'Filter\Explode'                     =>'Filter\Explode',
            'Filter\File\RenameUpload'           =>'Filter\File\RenameUpload',
            'Filter\FileSize'                    =>'Filter\FileSize',
            'Filter\ToFloat'                     =>'Filter\ToFloat',
            'Filter\IdFilter'                    =>'Filter\IdFilter',
            'Filter\Image'                       =>'Filter\Image',
            'Filter\ImagePathName'               =>'Filter\ImagePathName',
            'Filter\Implode'                     =>'Filter\Implode',
            'Filter\IntToBoolean'                =>'Filter\IntToBoolean',
            'Filter\IntToYesNo'                  =>'Filter\IntToYesNo',
            'Filter\JsTimestampToPhp'            =>'Filter\JsTimestampToPhp',
            'Filter\JsonDecode'                  =>'Filter\JsonDecode',
            'Filter\JsonEncode'                  =>'Filter\JsonEncode',
            'Filter\LineEndingToSeparator'       =>'Filter\LineEndingToSeparator',
            'Filter\ModuleControllerActionObject'=>'Filter\ModuleControllerActionObject',
            'Filter\NumberFormat'                =>'Filter\NumberFormat',
            'Filter\Pluralize'                   =>'Filter\Pluralize',
            'Filter\Phone'                       =>'Filter\Phone',
            'Filter\PhpTimestampToJs'            =>'Filter\PhpTimestampToJs',
            'Filter\QRCodeFilter'                =>'Filter\QRCodeFilter',
            'Filter\QueryParamToArray'           =>'Filter\QueryParamToArray',
            'Filter\QuoteString'                 =>'Filter\QuoteString',
            'Filter\Sanitize'                    =>'Filter\Sanitize',
            'Filter\SeparatorToArray'            =>'Filter\SeparatorToArray',
            'Filter\StringToImage'               =>'Filter\StringToImage',
            'Filter\StringToMongoId'             =>'Filter\StringToMongoId',
            'Filter\StringToMongoDate'             =>'Filter\StringToMongoDate',
            'Filter\StringTruncate'              =>'Filter\StringTruncate',
            'Filter\TinyCrypt\Encrypt'           =>'Filter\TinyCrypt\Encrypt',
            'Filter\ToString'                    =>'Filter\ToString',
            'Filter\TinyCrypt\Decrypt'           =>'Filter\TinyCrypt\Decrypt',
            'Filter\UpperCaseWords'              =>'Filter\UpperCaseWords',
            'Filter\UrlEncode'                   =>'Filter\UrlEncode',
            'Filter\UrlDecode'                   =>'Filter\UrlDecode',
            'Filter\Utf8'                        =>'Filter\Utf8',
            'Filter\YesNo'                       =>'Filter\YesNo',
            'Filter\CreditCard\NumberToType'     =>'Filter\CreditCard\NumberToType',
        ),
    ),

    /**
     * Filter MetaData
     */
    'filter_metadata' => array(
        'Filter\DefaultValue'=>array(
            'default_value'=>'string'
        ),
        'Filter\AngularProperties'=>array(),
        'Filter\BooleanizeJsonString'=>array(),
        'Filter\BooleanToInteger'=> array(
            'true_value' => 'int',
            'false_value' => 'int',
        ),
        'Filter\Currency'=> array(
            'show_decimal' => 'bool',
            'locale' => 'string',
            'currency_code'=>'string'
        ),
        'Filter\CurrentDomain'=> array(),
        'Filter\DatabaseTableName'=> array(),
        'Filter\Explode'=> array(
            'glue'=>'string'
        ),
        'Filter\DeleteFile'=> array(
            'throw_exceptions'=>'bool'
        ),
        'Filter\FileSize' => array(
            'precision'=>'int'
        ),
        'Filter\File\RenameUpload' => array(
            'strip_public'=>'bool',
            'overwrite' => 'bool',
            'randomize' => 'bool',
            'target' => 'string',
            'use_upload_extension' => 'bool',
            'auto_create_directory' => 'bool',
            'use_upload_name' => 'bool',
        ),
        'Filter\ToFloat'=> array(
            'decimals'=>'int',
        ),
        'Filter\Image'=> array(
            'mode'=>'string',
            'width'=>'int',
            'height'=>'int',
            'startX'=>'int',
            'startY'=>'int',
            'size'=>'string',
            'source_directory'=>'string',
            'destination_directory'=>'string',
            'destination_file_name'=>'string',
            'append_mode_and_size'=>'bool',
        ),
        'Filter\IdFilter'=>array(
            'crypt'=>'bool',
            'crypt_method'=>'string'
        ),
        'Filter\ImagePathName'=> array(
            'keep_public_directory'=>'bool'
        ),
        'Filter\Implode'=> array(
            'glue'=>'string'
        ),
        'Filter\IntToBoolean'=>array(),
        'Filter\IntToYesNo' => array(),
        'Filter\JsonDecode' => array(),
        'Filter\JsTimestampToPhp' => array(),
        'Filter\JsonEncode' => array(),
        'Filter\LineEndingToSeparator'=> array(
            'separator'=>'string',
            'wrap'=>'string',
        ),
        'Filter\ModuleControllerActionObject'=> array(
            'str_to_lower'=>'bool',
        ),
        'Filter\NumberFormat' => array(
            'decimals'=>'int',
            'decimal_point'=>'string',
            'thousands_separator'=>'string',
        ),
        'Filter\Pluralize'=>array(),
        'Filter\Phone'=>array(),
        'Filter\PhpTimestampToJs'=>array(),
        'Filter\QRCodeFilter'=> array(
            'destination'=>'string',
            'mode'=>'string',
            'file_name'=>'string',
            'size'=>'int',
            'use_value_as_file_name'=>'bool',
            'filter_return_path'=>'bool',
        ),
        'Filter\QuoteString'=>array(),
        'Filter\QueryParamToArray'=> array(
            'separator' => 'string'
        ),
        'Filter\Sanitize' => array(),
        'Filter\SeparatorToArray' => array(
            'separator' => 'string'
        ),
        'Filter\StringToImage' => array(
            'target'=>'string',
            'maximum_size'=>'string',
            'randomize'=>'bool',
            'correct_orientation'=>'bool',
            'resize_up'=>'bool',
            'quality'=>'int',
            'replace_public'=>'string',
            'auto_create_target_directory'=>'bool',
        ),
        'Filter\StringToMongoId'=> array(),
        'Filter\StringToMongoDate'=> array(),
        'Filter\StringTruncate'  => array(
            'string_length'=>'int',
            'use_word_wrap'=>true,
            'append_ellipsis'=>true,
        ),
        'Filter\TinyCrypt\Encrypt'=> array(
            'length'=>'int',
            'key'=>'string'
        ),
        'Filter\ToString'=> array(),
        'Filter\ToInt'=> array(),
        'Filter\TinyCrypt\Decrypt'=> array(
            'length'=>'int',
            'key'=>'string'
        ),
        'Filter\UpperCaseWords' => array(
            'string_to_lower'=>'bool'
        ),
        'Filter\UrlEncode' => array(),
        'Filter\UrlDecode' => array(),
        'Filter\Utf8' => array(
            'use_mb_string'=>'bool',
        ),
        'Filter\YesNo' => array(),
        'Filter\CreditCard\NumberToType' => array(),
    ),

    /**
     * View Helpers
     */
    'view_helpers'=>array(
        'invokables'=>array(
            'filter'=>'Filter\ViewHelper\Filter',
        ),
    ),
);
