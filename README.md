# Filter #

This module contains commonly used filters. These filters are available via the Filter Manager and in Apigility.
 
Filters can be invoked directly 

```$filter = new Currency();```

Via service and filter managers 

```$filter = $this->getServiceLocator()->get('Filter\Currency');``` 

or in view via a view helper 

```$this->filter('currency', 12.23, array('decimals'=>2));```

### Filters: ###

```php
        'Filter\AngularProperties'=>array(),
        'Filter\BooleanizeJsonString'=>array(),
        'Filter\BooleanToInteger'=> array(
            'true_value' => 'int',
            'false_value' => 'int',
        ),
        'Filter\Currency'=> array(
            'show_decimal' => 'bool',
            'locale' => 'string',
            'currency_code'=>'string'
        ),
        'Filter\CurrentDomain'=> array(),
        'Filter\DatabaseTableName'=> array(),
        'Filter\DeleteFile'=> array(
            'throw_exceptions'=>'bool'
        ),
        'Filter\FileSize' => array(
            'precision'=>'int'
        ),
        'Filter\File\RenameUpload' => array(
            'strip_public'=>'bool',
            'overwrite' => 'bool',
            'randomize' => 'bool',
            'target' => 'string',
            'use_upload_extension' => 'bool',
            'use_upload_name' => 'bool',
            'auto_create_directory' => 'bool',
        ),
        'Filter\ToFloat'=> array(
            'decimals'=>'int',
        ),
        'Filter\Image'=> array(
            'mode'=>'string',
            'width'=>'int',
            'height'=>'int',
            'startX'=>'int',
            'startY'=>'int',
            'size'=>'string',
            'source_directory'=>'string',
            'destination_directory'=>'string',
            'destination_file_name'=>'string',
            'append_mode_and_size'=>'bool',
        ),
        'Filter\IdFilter'=>array(
            'crypt'=>'bool',
            'crypt_method'=>'string'
        ),
        'Filter\ImagePathName'=> array(
            'keep_public_directory'=>'bool'
        ),
        'Filter\IntToBoolean'=>array(),
        'Filter\IntToYesNo' => array(),
        'Filter\JsonDecode' => array(),
        'Filter\JsTimestampToPhp' => array(),
        'Filter\JsonEncode' => array(),
        'Filter\LineEndingToSeparator'=> array(
            'separator'=>'string',
            'wrap'=>'string',
        ),
        'Filter\ModuleControllerActionObject'=> array(
            'str_to_lower'=>'bool',
        ),
        'Filter\NumberFormat' => array(
            'decimals'=>'int',
            'decimal_point'=>'string',
            'thousands_separator'=>'string',
        ),
        'Filter\Pluralize'=>array(),
        'Filter\Phone'=>array(),
        'Filter\PhpTimestampToJs'=>array(),
        'Filter\QRCodeFilter'=> array(
            'destination'=>'string',
            'mode'=>'string',
            'file_name'=>'string',
            'size'=>'int',
            'use_value_as_file_name'=>'bool',
            'filter_return_path'=>'bool',
        ),
        'Filter\QuoteString'=>array(),
        'Filter\QueryParamToArray'=> array(
            'separator' => 'string'
        ),
        'Filter\Sanitize' => array(),
        'Filter\SeparatorToArray' => array(
            'separator' => 'string'
        ),
        'Filter\StringToImage' => array(
            'target'=>'string',
            'maximum_size'=>'string',
            'randomize'=>'bool',
            'correct_orientation'=>'bool',
            'quality'=>'int',
            'replace_public'=>'string',
            'auto_create_target_directory'=>'bool',
        ),
        'Filter\StringToMongoId'=> array(),
        'Filter\StringTruncate'  => array(
            'string_length'=>'int',
            'use_word_wrap'=>true,
            'append_ellipsis'=>true,
        ),
        'Filter\TinyCrypt\Encrypt'=> array(
            'length'=>'int',
            'key'=>'string'
        ),
        'Filter\ToInt'=> array(),
        'Filter\ToString'=> array(),
        'Filter\TinyCrypt\Decrypt'=> array(
            'length'=>'int',
            'key'=>'string'
        ),
        'Filter\UpperCaseWords' => array(
            'string_to_lower'=>'bool'
        ),
        'Filter\UrlEncode' => array(),
        'Filter\UrlDecode' => array(),
        'Filter\Utf8' => array(
            'use_mb_string'=>'bool',
        ),
        'Filter\YesNo' => array(),
        'Filter\CreditCard\NumberToType' => array(),

```